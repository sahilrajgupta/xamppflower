<html>
   <head>
     <title>
	  buteamnsp
	 </title>
	</head>
	 <body>
<!--logo-->	 
	 <div style=" height:60px; margin-left:130px; margin-right:20px; border-radius:10px; background-color:#5A5D58;">
	 </div>
	 <div style=" width:90px; margin-top:-65px; margin-left:15px; height:65px; background-color:blue;">
	 <img src="images/flower2.jpg" alt="logo" style="width:90px; height:70px;"> </img>
<!--home button-->
	 <form action="main.php">
	 <input type="submit" name="home" value=" home" style="width:65px; font:size:18px; height:26px; margin-top:-35px; margin-left:1050px; ">
	 </div>
<!--btmsp-->
	 <div style=" height:155px; margin-left:60px; margin-right:60px; margin-top:20px; border-radius:10px; background-color:red;">
	 <img src="images/btmsp.jpg" alt="logo" style=" height:160px; width:1230px;  border-radius:10px;"> </img>
	 </div>
<!--heading-->
	  <h1 style="color:white; font-size:40px; margin-left:90px; margin-top:-100px;"> <u><i>butea monosperma flower </u></i></h1>
<!--info box-->
	 <div style=" background-color:white; border:1px solid black; width:250px; margin-left:60px; margin-top:90px; border-radius:10px; height:500px; ">
<!--buteamsp-->
<center><h1 style=" margin-left:8px; margin-right:8px; margin-top:5px; background-color:#E56608;">Butea Monosperma</h1></center>
	  <img src="images/butea1.jpg" alt="lotus pic" style=" height:160px; margin-left:8px; margin-top:-8px; width:234px;"> </img>
	  <center><h3 style=" margin-left:5px; font-size:23px;  margin-right:5px; background-color:#E56608; "> Scientific classifications</h3></center>
	 <p style=" margin-left:8px; font-size:20px;">Kingdom:	&nbsp&nbspPlantae<br>
Clade:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	Angiosperms<br>
Clade:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	Eudicots<br>
Order:	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Fabales<br>
Family:&nbsp&nbsp&nbsp&nbsp&nbsp		Fabaceae<br>
Genus:	&nbsp&nbsp&nbsp&nbsp&nbsp 	Butea<br>
Species:&nbsp&nbsp&nbsp&nbsp	B. monosperma<br>
</p>
	 </div>
	 <br>
<!--para1-->
	 <p style=" margin-left:350px; font-size:20px; margin-top:-500px;"> Butea monosperma is a species of Butea native to tropical and sub-tropical parts of the Indian<br>
                                                                    	 Subcontinent and Southeast Asia, ranging across India, Bangladesh, Nepal, Sri Lanka, Myanmar, <br>
																		 Thailand, Laos, Cambodia, Vietnam, Malaysia, and western Indonesia. Common names include <br>
                                                                       	 flame-of-the-forest and bastard teak.  																 
	 </p>
	 <!--para2-->
	<p style=" margin-left:350px; font-size:20px;">            <br>It is a medium-sized dry season-deciduous tree, growing to 15 m (49 ft) tall. It is a slow growing tree,<br>
                                                                            	young trees have a growth rate of a few feet per year. The leaves are pinnate, with an 8–16 cm (3.1–6.3 in)<br>
																				petiole and three leaflets, each leaflet 10–20 cm (3.9–7.9 in) long. The flowers are 2.5 cm (0.98 in) long,<br>
																				bright orange-red, and produced in racemes up to 15 cm (5.9 in) long. The fruit is a pod 15–20 cm <br>
																				(5.9–7.9 in) long and 4–5 cm (1.6–2.0 in) broad.
	</p>
	
	<div style=" margin-left:350px; border-radius:10px; background-color:#5A5D58; width:850px; height:30px; margin-top:">
<!--uses-->
	<h2 style="color:white;">&nbsp&nbsp&nbsp Uses</h2>
	</div>
	<p style=" margin-left:350px; font-size:20px;">                It is used for timber, resin, fodder, medicine, and dye. The wood is dirty white and soft and, being durable<br>
                                                                 	under water, is used for well-curbs and water scoops. Spoons/Ladles made of this tree are used in various Hindu<br>
																	rituals to pour Ghee (clarified butter) into the fire. Good charcoal can be obtained from it. The leaves are usually <br>
																	very leathery and not eaten by cattle. The leaves were used by previous generations of people to serve food <br>
																	instead of plastics of today.
	
	</p>
	
	</body>
</html>
		 