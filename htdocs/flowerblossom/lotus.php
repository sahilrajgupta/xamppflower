<html>
   <head>
     <title>
	  LOTUS
	 </title>
	</head>
	 <body style=" ">
<!--logo-->
	 <div style=" height:60px; margin-left:130px; margin-right:20px; border-radius:10px; background-color:#5A5D58;">
	 </div>
	 <div style=" width:90px; margin-top:-65px; margin-left:15px; height:65px; background-color:blue;">
	 <img src="images/flower2.jpg" alt="logo" style="width:90px; height:70px;"> </img>
<!--home button-->	 
	 <form action=" main.php">
	 <input type="submit" name="home" value=" home" style="width:65px; font:size:18px; height:26px; margin-top:-35px; margin-left:1050px; ">
	 </div>
<!--lotus image-->
	 <div style=" height:155px; margin-left:60px; margin-right:60px; margin-top:20px; border-radius:10px; background-color:red;">
	 <img src="images/lotus.jpg" alt="logo" style=" height:160px; width:1230px;  border-radius:10px;"> </img>
	 </div>
<!--heading-->
	      <h1 style="color:white; font-size:40px; margin-left:90px; margin-top:-100px;"> <u><i>lotus flower </u></i></h1>
	      <h2 style="color:white; margin-left:90px; margin-top:-25px;"> From the mud of adversity grows the lotus of joy! </h2>
<!--infobox-->
	 <div style=" background-color:white; border:1px solid black; width:250px; margin-left:60px; margin-top:90px; border-radius:10px; height:500px; ">
<!--lotus-->
<center><h1 style=" margin-left:8px; margin-right:8px; margin-top:5px; background-color:#4C841F; color:white;">lotus</h1></center>
	  <img src="imageslotus1.jpg" alt="lotus pic" style=" height:160px; margin-left:8px; margin-top:-8px; width:234px;"> </img>
	  <center><h3 style=" margin-left:5px; font-size:23px;  margin-right:5px; background-color:#4C841F; color:white; "> Scientific classifications</h3></center>
	 <p style=" margin-left:8px; font-size:20px;">Kingdom:	&nbsp&nbspPlantae<br>
Clade:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	Angiosperms<br>
Clade:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp	Eudicots<br>
Order:	&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspProteales<br>
Family:&nbsp&nbsp&nbsp&nbsp&nbsp	Nelumbonaceae<br>
Genus:	&nbsp&nbsp&nbsp&nbsp&nbsp Nelumbo<br>
Species:&nbsp&nbsp&nbsp&nbsp	N. nucifera<br>
</p>
	 </div>
	 <br>
<!--info-->
	<p style=" margin-left:350px; font-size:20px; margin-top:-500px;"> <b>Nelumbo</b> is a genus of aquatic plants with large, showy flowers.
                                	Members are commonly called lotus,<br> though "lotus" is a name also applied 
									to various other plants and plant groups, including the <br>unrelated genus Lotus.
									Members outwardly resemble those in the family Nymphaeaceae ("water lilies"),<br>
									but Nelumbo is actually very distant to Nymphaeaceae. <br><br>
									There are only two known living species of lotus; Nelumbo nucifera is native to Asia and is better-known. 
									<br>It is commonly cultivated; it is eaten and used in traditional Chinese medicine.<br>
									This species is the floral emblem of both India and Vietnam.<br><br>

                                    The other lotus is Nelumbo lutea and is native to North America and the Caribbean. Horticultural hybrids<br> 
									have been produced between these two allopatric species.
									</p>
	<div style=" margin-left:350px; border-radius:10px; background-color:#5A5D58; width:850px; height:30px; margin-top:-6px;">
<!--list of extantspecies-->
	<h2 style="color:white;">&nbsp&nbsp&nbsp Extant Species</h2>
		</div>
		<ul style=" margin-left:346px; font-size:20px; margin-top:20px;">
	             <li><b>Nelumbo lutea Willd.</b> – American lotus (Eastern United States, Mexico, Greater Antilles, Honduras)</li>
                 <li><b>Nelumbo nucifera Gaertn.</b> – sacred or Indian lotus, also known as the Rose of India and the sacred<br> water lily of Hinduism and Buddhism.
				 It is the national flower of India and Vietnam. Its roots and seeds <br>are also used widely in Asian cooking.</li>
        </ul>	
	 </body>
</html>