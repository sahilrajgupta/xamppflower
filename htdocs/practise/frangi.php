<html>  
   <head>
       <title>
	       frangipani
       </title>
	   
	   <style>
	   p
	   {
		   color:black;
		   font-size:20px;
	   }
	   </style>
   </head>
<!--backgroundimage-->   
   <body style=" background-image:url('.jpg'); background-size:cover;">
  
<!--heading-->
  <div style= "height:55px; background-color:white; background-color:#D2D763; margin-top:40px; box-sizing: border-box;">
	    <h1 style="color:black; font-size:45px;"> &nbsp&nbsp&nbsp&nbsp&nbsp <u>frangipani </u></h1>
<!--homebutton-->	
  <div style=" width:50px; height:25px; margin-left:1000px;margin-top:-60px;">
	<form action=" flower.php">
	    <input type="submit" name="save" value="HOME" >
    </form>
  </div>
  </div>
	
<!--1stpara-->	
<p> <br> &nbsp&nbsp&nbsp&nbsp&nbsp  Plumeria (common name Frangipani) is a small genus of 7-8 species that grow in tropical   <br> 
      &nbsp&nbsp&nbsp&nbsp&nbsp     and subtropical America. The genus consists of mainly deciduous shrubs and trees.<br>
	 &nbsp&nbsp&nbsp&nbsp&nbsp	 P. rubra (Common Frangipani, Red Frangipani), that grows in Mexico, Central America,and <br>
	&nbsp&nbsp&nbsp&nbsp&nbsp	 Venezuela, produces flowers ranging from white, yellow to pink. From Mexico and Central <br>
	&nbsp&nbsp&nbsp&nbsp&nbsp America, Plumeria has spread to all tropical areas of the world, especially Hawaii, where<br>
	&nbsp&nbsp&nbsp&nbsp&nbsp	 it grows so abundantly that many people think that it is native there.
</p>
<!--2ndpara-->
<p><br>&nbsp&nbsp&nbsp&nbsp&nbsp Plumeria flowers are most fragrant at night in order to lure sphinx moths to pollinate them.<br>
                                                                        &nbsp&nbsp&nbsp&nbsp&nbsp   The flowers yield no nectar,  however, and simply trick their pollinators. The moths inadvertently<br>
																		&nbsp&nbsp&nbsp&nbsp&nbsp   pollinate them by transferring pollen from flower to flower in their fruitless search for nectar. <br>
																		&nbsp&nbsp&nbsp&nbsp&nbsp   Insects or human pollination can help create new varieties of plumeria. Plumeria trees from cross<br>
																	&nbsp&nbsp&nbsp&nbsp&nbsp 	  pollinated seeds may show characteristics of the mother tree or their flowers  might just have a  <br>
																		&nbsp&nbsp&nbsp&nbsp&nbsp   totally new look.
</p> 
<!--3rdpara-->
<p> <br> &nbsp&nbsp&nbsp&nbsp&nbspPlumeria species may be propagated easily by cutting leafless stem tips in spring. Cuttings are<br>
                                 &nbsp&nbsp&nbsp&nbsp&nbsp  allowed to dry at the base before planting in well-drained soil. Cuttings are particularly <br>
								 &nbsp&nbsp&nbsp&nbsp&nbsp  susceptible to rot in moist soil.

</p>
<!--detailbox-->
  <div style=" margin-left:1000px; margin-right:40px; height:430px; margin-top:-470px; background-color:white; border:1px solid black;">
    <h1 style=" background-color:red; color:black; margin-left:4px;"> Plumeria</h1>
  </div>
   </body>
</html>